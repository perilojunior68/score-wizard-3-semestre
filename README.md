# 3º Semestre - ScoreWizard
Em parceria com o SPC Brasil, foi proposto o desenvolvimento de uma ferramenta capaz de obter análises estatísticas da evolução do consumidor quando comparado ao seu históricos de pagamentos de créditos, levando em conta também sua pontuação de Score. <br>
A ferramenta poderá ser utilizada por pessoas físicas e jurídicas deste que estas possuam o cadastro positivos. O sistema também possui o objetivo de fornecer educação financeira, contribuindo para melhora de Score e permitir para que futuramente o usuário possa conseguir novos créditos.

## Objetivos
De inicio, foi realizado um levantamento de metodologias para o melhor cálculo de Score, com a finalidade de obter o mesmo da forma mais condizente com a realidade.
Foi fornecido pelo cliente diversos dados dos usuários para que seja realizado o cadastro. Com base nestes dados foi definido a ideia "gamificação", onde o usuário possuísse objetivos a serem cumpridos para que seu Score melhorasse. Além de conter uma mecânica de XP (experiência) para um melhor incentivo para concluir os objetivos.
É possível definir os objetivos nos seguintes tópicos:
- Cadastro de pessoas físicas e jurídicas <br>
Com os dados que recebemos, o sistema é capaz de cadastrar os usuários em massa no sistema
- Calcular o Score dos usuários <br>
Após o cadastro, foi levantada a melhor maneira do cálculo deste valor para todos os usuários cadastrados do sistema, levando em conta sua idade, inadimplências e quantidade de solicitações de Score
- Criação de metas para serem concluídas <br>
Durante a criação do escopo do cálculo, foi levantado os itens que faziam o score do usuário diminuir, estes itens que foram se transformados objetivos para gamificação do projeto.

## Tecnologias utilizadas
* Java:  Buscando atender as requisições do cliente, o Java foi utilizado visando sua extensa biblioteca de frameworks.
* Spring: Por votação a framework Spring foi utilizada para criação da aplicação.
* Maven: para o auxílio da inclusão de bibliotecas de maneira mais fácil.
* HTML: para criação de templates.
* JavaScript: utilizado para implementação de funcionalidades Front-end.
* CSS: para criar uma interface mais amigável e intuitiva.
As tecnologias foram escolhidas visando o apoio do docente da Fatec, como em grande maioria as aulas utilizavam Java foi identificada uma possiblidade de aperfeiçoamento na linguagem escolhida.

## Contribuições individuais
Diferentemente dos outros semestres, durante a concepção do Score Wizard, atuei como recurso de apoio para todas as áreas do projeto. Exercendo um trabalho como ferramenta facilitadora para todos os pontos de risco durante o desenvolvimento do projeto.

### Contribuição na implementação
- Apoio planejamento de Sprints <br>
Atuei como apoio ao master para definir os entregáveis da última Sprint, priorizando os ajustes finais e a entrega da última implementação para o cliente
<div align="center">
    <img src="./Projeto/sprint3.PNG" width="720" />
</div>

- Atuei como auxílio na lógica para o sistema de login do projeto
```python
# Rota de endpoint da requisição HTTP
 @PostMapping("/index")
	  public String LoginSubmit(@ModelAttribute Login greeting, Model model) {
		 	
			#Retorna todas pessoas físicas cadastradas no sistema
		 	ArrayList<PessoaFisica> pessoaf = new ArrayList<>();
		 	pessoaf.addAll(pf.getAllPessoaFisica());
		 	
			#Bloco de repetição para encontrar um usuário cadastrado
		 	int i = 1;
		 	while( i <= pessoaf.size()) {
				#Verifica se existe algum usuário registrado para as credenciais envolvidas
			  	  if(greeting.getCpf().equals(pessoaf.get(i).getDocumento()) && greeting.getSenha_usu().equals(pessoaf.get(i).getSenha())) {
			  		  System.out.println("User j cadastrado");
					  #Chamada para calculo do XP do usuário
			  		  pf.getCalcularXP(pessoaf.get(i).getDocumento());
					  #Retorna o dashboard inicial
			  		  return "dashboard";
			  	  }
			  	  i++;
			}

	    return null;	  
	  }
```

- Criação da regra de negócio do Score pessoal <br>
O número real do Score do usuário não foi passado para a equipe, por este motivo decidimos pesquisar e idealizar um cálculo de Score de cada pessoa física registrada no sistema. Para isso, utilizamos  a seguinte [fonte](https://marbled-range-ed0.notion.site/Entenda-o-que-comp-e-o-c-lculo-do-Score-1348e1d22dbb49098a775e9d973c86e4) para basearmos nossos cálculos.
Desta forma, foi passado para equipe de maneira procedural os seguintes itens que afetariam na pontuação do Score da pessoa:
 1. Dados públicos, como pesquisas de mercado de trabalho, índice de endividamento por região, entre outros;
 2. Dados pessoais fornecidos em cadastros (RG, CPF, endereço e data de nascimento);
3.  Quantas vezes buscou crédito no mercado;
4.   Se houve busca de crédito em financeiras;
5.   Idade desde a primeira busca de crédito;
6.   Tempo desde a primeira busca de crédito paga por meio de cheque;
7.   Número de vezes em que teve dívidas em aberto;
8.   Tempo da última exclusão da dívida em aberto;
9.   Valor de dívidas em aberto;
10.   Lugares onde os débitos foram registrados;
11.  Existência ou não de ações judiciais como busca e apreensão;
12.   Títulos protestados.
- Trabalho no Front-end <br>
Também realizei apoio para a equipe de Front-end, atuando como Web Designer e backup para este linha de frente do projeto.
> Tela de inicial de login
<div align="center">
    <img src="./Projeto/loginwiz.png" width="720" />
</div>

> Tela de cadastro
<div align="center">
    <img src="./Projeto/cadastrowiz.png" width="720" />
</div>

> Tela de seleção de objetivos
<div align="center">
    <img src="./Projeto/selecaowiz.png" width="720" />
</div>

- Deploy no Heroku <br>
Durante a reta final do projeto, participei de maneira ativa na migração do projeto para a plataforma Heroku.
Esta plataforma permitia executar todo nosso projeto em Nuvem, onde o usuário poderia realizar testes na aplicação e enviar feedback de melhorias para serem aplicadas na próxima Sprint.
Uma vez o CLI do Heroku instalado e configurado no git, bastava apenas o simples comando de GIT para que o mesmo fosse enviado ao servidor:
```
git  add  .
git  commit -am  "Mensagem de commit"
git  push  heroku  master
```
Ao ser enviado, bastava recarregar a página para verificar as atualizações.
## Aprendizados efetivos
- Deploy no Heroku<br>
Como parte dos requisitos do cliente, realizamos o envio do projeto ao Heroku, neste tópico consegui absorver todo o caminho para enviar um projeto Java com base de dados MySql ao Heroku
- Cálculo do Score<br>
Conforme informado dentro do tópico de contribuições individuais, realizei a pesquisa e segregação de ações que aumentariam e diminuiriam o Score de usuários no sistema.
- Novos aprendizados de Front-end<br>
Neste semestre, realizamos a utilização de CSS puro e a framework Front-end Bootstrap 4 para criação dos layouts. Com esta utilização foi possível identificar a facilidade e agilidade de lidar com estilização agradável principalmente na criação de formulários.
- Utilização do Maven
Assim como o Gradle, o conhecimento referente a utilização do Maven foi bem utilizado e com sua integração foi possível observar a importância do mesmo para um projeto com várias pessoas envolvidas.